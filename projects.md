## Current projects

TODO: this is just a raw list of projects ordered by most recent first. I need to clean things up and add more comments and screenshots to show what I learned and why I developed them.

### Primary projects
#### Delibay

#### PLX

### Secondary projects
Kinda working on it when I have time and motivation, or when a friend wants to learn a new tool I know but hasn't documented yet...

#### PRJS

#### productivity
A git repository to add cheatsheets and practice opportunities to discover productivity tools and get started with a simple practice driven initiation.. There is a mix of french and english notes.
#### setup
My Fedora setup almost fully automated with Ansible !

## Archived projects
These are previous project that have been done or most often interrupted by another new project idea or for other reasons. Here are a few details of the history of the project, just for reference and memories, what technologies I used and learned along the way, and finally why/how it ended.

### LXSetup

**Relevant links**
**Technologies**
**Problems**
**Solutions**
**How it ended?**

### CTP
**Relevant links**
- [GitHub repository](https://github.com/samuelroland/ctp)

**Technologies**
- Docker wrapper
- Java and Python

**Problems**
**Solutions**
**How it ended?**

### APDebug
**Relevant links**
- [Codeberg repository](https://codeberg.org/samuelroland/apdebug)

**Technologies**
- Docker wrapper
- Java and Python

**Problems**
**Solutions**
**How it ended?**

### openDevApps

**Relevant links**
- [GitHub repository](https://github.com/samuelroland/openDevApps)
- [Addon page on `addons.mozilla.org`](https://addons.mozilla.org/addon/opendevapps)

**Technologies**
- VueJS

**Problems**

**Solutions**
**How it ended?**

### Podz
**Relevant links**
- [GitHub repository](https://github.com/samuelroland/podz)
- [Project resume (FR)](https://github.com/samuelroland/podz/blob/main/docs/podz-r%C3%A9sum%C3%A9-tpi.pdf)
- [Final report (FR)](https://github.com/samuelroland/podz/blob/main/docs/podz-docs.pdf)
- [Work diary (FR)](https://github.com/samuelroland/podz/blob/main/docs/podz-journal.pdf)


### KanFF - 
**Relevant links**
- Main GitHub repository: [KanFF](https://github.com/samuelroland/KanFF)
- [Wayback machine version of kanff.org](https://web.archive.org/web/20210812043608/https://kanff.org/)
- Website GitHub repository: [KanFF](https://github.com/KanFF/kanff.org)

**Technologies**
- PHP
- JS
- JQuery
- HTML and CSS

All in vanilla ways, mostly without any dependency except the Markdown parser, learning a framework like Laravel and VueJS made all this work a bit obsolete...

**Problems**
TODO

**Solutions**
TODO

**How it ended?**
